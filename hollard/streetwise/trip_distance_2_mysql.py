import requests
import json
import mysql.connector
from mysql.connector import Error
from mysql.connector import errorcode

#import sys 
#folder = sys.argv

from configobj import ConfigObj
config = ConfigObj('./config.ini')

def jprint(obj):
    # create a formatted string of the Python JSON object
    text = json.dumps(obj, sort_keys=True, indent=4)
    print(text)

req_name = 'get_pin_gate_distances'

with open('./widget_ids.txt', 'r') as file:
    widget_ids = file.read().replace('\n', '')


args = '%7B%22widget_id_list%22:[{id_list}],%22platform%22:%22produk%22%7D'.format(id_list = widget_ids)
#jprint(args)

headers = {'Content-Type': 'application/json',
           'Authorization': 'Bearer {0}'.format(config["api"]["token"])}

import_log_sql = ''' INSERT INTO results_import_log(doc, msg, imp_date) values (%s, %s, NOW()); '''           

req = '{0}?request={1}&args={2}'.format(config["api"]["url_base"], req_name, args)
#jprint(req)

response = requests.get(req, headers=headers)

#jprint(response.json())
print (response.json()["data"]["count"])

data = response.json()["data"]["documents"]
out = ''
#exc stored proc
try:

    connection = mysql.connector.connect(host = config['mysql']['host'],
                                         port = config['mysql']['port'],
                                         database = config['mysql']['database'],
                                         user = config['mysql']['user'],
                                         password = config['mysql']['password'],
                                         autocommit = True )

    cursor = connection.cursor()

    for profile in data:

        if 'widget_id' in profile and 'device_profile_id' in profile:
            project = config["general"]["project"]
        else:
            vals = (json.dumps(profile), 'No suitable id field found' )
            cursor.execute(import_log_sql, vals)
            #connection.commit()
            print('No suitable id fields found')
            #continue

        distances = profile["distances"].items()

        for key, value in distances:
            start_dist = value["start"]
            end_dist = value["end"]
            odo = value["odo_reading"]
            #print(odo)
            trip_name = ''
            if "trip_name" in value:
                trip_name = value["trip_name"]

            spargs = [
                profile["widget_id"],
                profile["widget_profile_id"],
                profile["device_profile_id"],
                key,
                profile["profile_id"],
                profile["uuid"],
                profile["journey_name"],
                profile["full_name"],
                profile["email"],
                profile["mobile"],
                profile["government_identification_number"],
                profile["employee_code"],
                trip_name,
                start_dist["latitude"],
                start_dist["longitude"],
                start_dist["date"],
                odo["odo_start"],
                odo["dt_start"],
                value["start_address"],
                end_dist["latitude"],
                end_dist["longitude"],
                end_dist["date"],
                odo["odo_end"],
                odo["dt_end"],
                value["end_address"],
                odo["odo_distance"],
                value["distance"],
                project,
                out
            ]
            #print (spargs)
        
            res_args = cursor.callproc('upsert_distance', spargs)
            #print(res_args[28])
            if 'updated' not in res_args[28] and 'inserted' not in res_args[28]:
                vals = (json.dumps(profile), res_args[28] )
                cursor.execute(import_log_sql, vals)
    

except mysql.connector.Error as error:
    print("Failed to execute stored procedure: {}".format(error))
finally:
    if (connection.is_connected()):
        cursor.close()
        connection.close()
        print("MySQL connection is closed")
