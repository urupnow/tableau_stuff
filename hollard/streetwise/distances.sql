drop table distances;

create table distances(
    widget_id           VARCHAR(255),
    widget_profile_id   VARCHAR(255),
    device_profile_id   VARCHAR(255),
    cycle_nbr           INT,
    profile_id          VARCHAR(255),
    uuid                VARCHAR(255),
    journey_name        VARCHAR(255),
    full_name           VARCHAR(255),
    email               VARCHAR(255),
    mobile              VARCHAR(255),
    id_number           VARCHAR(255),
    employee_code       VARCHAR(255),
    trip_name           VARCHAR(255),
    start_latitude      FLOAT,
    start_longitude     FLOAT,
    start_time          TIMESTAMP,
    start_odo           INT,
    start_odo_time      TIMESTAMP,
    start_address       VARCHAR(255),
    end_latitude        FLOAT,
    end_longitude       FLOAT,
    end_time            TIMESTAMP,
    end_odo             INT,
    end_odo_time        TIMESTAMP,
    end_address         VARCHAR(255),
    odo_distance        FLOAT,
    gps_distance        FLOAT,
    import_date         DATE,
    nbr_repeats         INT,
    comment             VARCHAR(255),
    project             VARCHAR(255)
);

drop procedure upsert_distance;

create procedure upsert_distance(
    IN      p_widget_id           VARCHAR(255),
    IN      p_widget_profile_id   VARCHAR(255),
    IN      p_device_profile_id   VARCHAR(255),
    IN      p_cycle_nbr           INT,
    IN      p_profile_id          VARCHAR(255),
    IN      p_uuid                VARCHAR(255),
    IN      p_journey_name        VARCHAR(255),
    IN      p_full_name           VARCHAR(255),
    IN      p_email               VARCHAR(255),
    IN      p_mobile              VARCHAR(255),
    IN      p_id_number           VARCHAR(255),
    IN      p_employee_code       VARCHAR(255),
    IN      p_trip_name           VARCHAR(255),
    IN      p_start_latitude      FLOAT,
    IN      p_start_longitude     FLOAT,
    IN      p_start_time          TIMESTAMP,
    IN      p_start_odo           INT,
    IN      p_start_odo_time      TIMESTAMP,
    IN      p_start_address       VARCHAR(255),
    IN      p_end_latitude        FLOAT,
    IN      p_end_longitude       FLOAT,
    IN      p_end_time            TIMESTAMP,
    IN      p_end_odo             INT,
    IN      p_end_odo_time        TIMESTAMP,
    IN      p_end_address         VARCHAR(255),
    IN      p_odo_distance        FLOAT,
    IN      p_gps_distance        FLOAT,
    IN      p_project             VARCHAR(255),
    OUT     p_msg                 VARCHAR(255)
)
sp: BEGIN
    declare v_rec_count  INT;

    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
        --ROLLBACK;
        GET DIAGNOSTICS CONDITION 1
    		@p2 = MESSAGE_TEXT;
    		SELECT @p2 into p_msg;
        --SELECT CONCAT('An error has occurred, operation rolled back and the stored procedure was terminated for ', p_full_name, ) INTO msg;
    END;

    SELECT COUNT(*) INTO v_rec_count FROM distances
    WHERE widget_id = p_widget_id
    AND   device_profile_id = p_device_profile_id
    AND   widget_profile_id = p_widget_profile_id
    AND   cycle_nbr = p_cycle_nbr;

    select v_rec_count;

    if v_rec_count > 0 then

        UPDATE distances 
        SET     profile_id = coalesce(p_profile_id,profile_id),
                uuid = coalesce(p_uuid, uuid),
                journey_name = coalesce(p_journey_name, journey_name),
                full_name = coalesce(p_full_name, full_name),
                email = coalesce(p_email, email),
                mobile = coalesce(p_mobile, mobile),
                id_number = coalesce(p_id_number, id_number),
                employee_code = coalesce(p_employee_code, employee_code),
                trip_name = coalesce(p_trip_name, trip_name),
                start_latitude = p_start_latitude,
                start_longitude = p_start_longitude,
                start_time = p_start_time,
                start_odo = p_start_odo,
                start_odo_time = p_start_odo_time,
                start_address = p_start_address,
                end_latitude = p_end_latitude,
                end_longitude = p_end_longitude,
                end_time = p_end_time,
                end_odo = p_end_odo,
                end_odo_time = p_end_odo_time,
                end_address = p_end_address,
                odo_distance = p_odo_distance,
                gps_distance = p_gps_distance,
                import_date = NOW(),
                nbr_repeats = nbr_repeats + 1,
                project = p_project
        WHERE   widget_id = p_widget_id
        AND     device_profile_id = p_device_profile_id
        AND     widget_profile_id = p_widget_profile_id
        AND     cycle_nbr = p_cycle_nbr;

        select CONCAT('device_profile_id: ', p_device_profile_id, ', widget_profile_id: ', p_widget_profile_id, ', widget_id: ', p_widget_id, ' updated.') INTO p_msg;
         
    ELSE

        INSERT INTO distances (
            widget_id,
            widget_profile_id,
            device_profile_id,
            cycle_nbr,           
            profile_id,
            uuid,
            journey_name,
            full_name,
            email,
            mobile,
            id_number,
            employee_code,
            trip_name,
            start_latitude,
            start_longitude,
            start_time,
            start_odo,
            start_odo_time,
            start_address,
            end_latitude,
            end_longitude,
            end_time,
            end_odo,
            end_odo_time,
            end_address,
            odo_distance,
            gps_distance,
            import_date,
            nbr_repeats,
            project
        ) VALUES (
            p_widget_id,
            p_widget_profile_id,
            p_device_profile_id,
            p_cycle_nbr,           
            p_profile_id,
            p_uuid,
            p_journey_name,
            p_full_name,
            p_email,
            p_mobile,
            p_id_number,
            p_employee_code,
            p_trip_name,
            p_start_latitude,
            p_start_longitude,
            p_start_time,
            p_start_odo,
            p_start_odo_time,
            p_start_address,
            p_end_latitude,
            p_end_longitude,
            p_end_time,
            p_end_odo,
            p_end_odo_time,
            p_end_address,
            p_odo_distance,
            p_gps_distance,
            NOW(),
            1,
            p_project
        );

        select CONCAT('device_profile_id: ', p_device_profile_id, ', widget_profile_id: ', p_widget_profile_id, ', widget_id: ', p_widget_id, ' inserted.') INTO p_msg;

    END IF;

END;