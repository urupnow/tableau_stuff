from pymongo import MongoClient
from bson import ObjectId
from bson import json_util
#import pandas as pd

# connect to MongoDB, change the << MONGODB URL >> to reflect your own connection string
uri = "mongodb://urupuser:vc28rbMyEZj#^TfD@mongo-rs-local-produs-srv1.urup.com,mongo-rs-local-produs-srv2.urup.com,mongo-rs-local-produs-srv3.urup.com:27017/urup?authSource=admin&replicaSet=produs_local_rs"

client = MongoClient(uri)
#urupuser:Md5#M~{5cs#5zjnd@mongospreview.urup.com:27017/urup?auto_reconnect=true&authSource=admin
db=client.urup

import json

# define the name of the file to read from
filename = "./veneer_profiles.json"

# open the file for reading
filehandle = open(filename, 'w')

# make an API call to the MongoDB server
cursor = db.veneer_profiles.find()

# extract the list of documents from cursor obj
mongo_docs = list(cursor)

# create an empty DataFrame for storing documents
#docs = pd.DataFrame(columns=[])
docs = []

# iterate over the list of MongoDB dict documents
for doc in mongo_docs:
    js_doc = json_util.dumps(doc) +'\n'
    #js_doc["_id"] = str(doc["_id"])
    # append the MongoDB Series obj to the DataFrame obj
    #docs.append(js_doc)
    filehandle.write(js_doc)

# export the MongoDB documents as a JSON file
    #txt = json.dumps(res) + '\n'
    #text_file.write(txt)

#json.dumps(docs, filehandle)
#filehandle.write(json.dumps(docs))

filehandle.close()
client.close()


#import ito mongdb
#mongoimport -v --host mongostest.urup.com:27017 --username urupuser --password Md5#M~{5cs#5zjnd -d "urup" -c "produs_veneer_profiles" --file /home/stephan/projects/tableau_stuff/produs/veneer_profiles.json --type json --authenticationDatabase admin