import requests
import json
import mysql.connector
from mysql.connector import Error
from mysql.connector import errorcode

from configobj import ConfigObj
config = ConfigObj('./config.ini')

def jprint(obj):
    # create a formatted string of the Python JSON object
    text = json.dumps(obj, sort_keys=True, indent=4)
    print(text)

with open('./widget_ids.txt', 'r') as file:
    widget_ids = file.read().replace('\n', '')

req_name = 'get_monitor_data_2'

args = '%7B%22widget_id_list%22:[{id_list}],%22extended_data%22:true,%22platform%22:%22produk%22,%22flatten%22:true%7D'.format(id_list = widget_ids)
#jprint(args)

req = '{0}?request={1}&args={2}'.format(config["api"]["url_base"], req_name, args)
#jprint(req)

headers = {'Content-Type': 'application/json',
           'Authorization': 'Bearer {0}'.format(config["api"]["token"])}

response = requests.get(req, headers=headers)

print (response.json()["data"]["count"])

data = response.json()["data"]["profiles"]
out = ''
#exc stored proc
try:

    connection = mysql.connector.connect(host = config['mysql']['host'],
                                         port = config['mysql']['port'],
                                         database = config['mysql']['database'],
                                         user = config['mysql']['user'],
                                         password = config['mysql']['password'],
                                         autocommit = True )

    cursor = connection.cursor()

    import_log_sql = ''' INSERT INTO results_import_log(doc, msg, imp_date) values (%s, %s, NOW()); ''' 

    for profile in data:

        if 'widget_id' in profile and 'device_profile_id' in profile:
            project = config["general"]["project"]
        else:
            vals = (json.dumps(profile), 'No suitable id field found' )
            cursor.execute(import_log_sql, vals)
            #connection.commit()
            print('No suitable id fields found')
            #continue
            
        if 'display_name' in profile:
            disp_name = profile['display_name']
        else: 
            disp_name = None

        spargs = [
            project,
            profile["widget_id"],
            profile["uuid"],
            profile["device_profile_id"],
            profile["widget_profile_id"],
            profile["full_name"],
            profile["email"],
            profile["mobile"],
            profile["id_number"],
            profile["employee_code"],
            disp_name,
            profile["nbr_gates"],
            profile["pct_complete"],
            profile["nbr_correct"],
            profile["pct_correct"],
            profile["engagement_seconds"],
            profile["gate_seconds"],
            profile["face_time_seconds"],
            profile["start"],
            profile["end"],
            profile["lock_gate"],
            profile["video_seconds"],
            profile["video_pct"],
            profile["channels_text"],
            profile["total_nbr_gates"],
            profile["total_nbr_questions"],
            out
        ]
        #print (spargs)
        
        res_args = cursor.callproc('upsert_monitor_results', spargs)
        #print(res_args[25])
        if 'updated' not in res_args[25] and 'inserted' not in res_args[25]:
            vals = (json.dumps(profile), res_args[25] )
            cursor.execute(import_log_sql, vals)

        
except mysql.connector.Error as error:
    print("Failed to execute stored procedure: {}".format(error))
finally:
    if (connection.is_connected()):
        cursor.close()
        connection.close()
        print("MySQL connection is closed")
