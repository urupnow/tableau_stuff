from pymongo import MongoClient
from pymongo import errors
from bson import ObjectId
import csv

import sys
rel_data_path = sys.argv[1]

import os
base_path = os.path.dirname(__file__)
data_path = os.path.join(base_path, rel_data_path)
print(data_path)

from configobj import ConfigObj
config = ConfigObj(data_path + "config.ini")

import datetime
date_object = datetime.date.today()

print(config['participant_field_map']['ancestor_emp_codes'])

# connect to MongoDB
uri = config['mongo']['url']

client = MongoClient(uri)
db=client.urup

# open the file for reading
file_name = data_path + config['files']['sub_folder'] + config['files']['participants']
print(file_name)

with open(file_name) as csvfile:
    data = list(csv.reader(csvfile, delimiter=config['general']['csv_delimiter']))

def assign_arg(conf_item):
    try:
        if (len(conf_item) >= 0):
            return conf_item
        else: 
            return None
    except ValueError:
        return None


rownbr = 0
hdr = []
for line in data:
    # read a single line
    if rownbr == 0:
        hdr = line
        print(hdr)
    else:
        doc={
            'organisation': assign_arg(line[int(config['participant_field_map']['org'])]),
            'emp_code': assign_arg(line[int(config['participant_field_map']['emp_code'])]),
            'full_name': assign_arg(line[int(config['participant_field_map']['name'])]),
            'email': assign_arg(line[int(config['participant_field_map']['email'])]),
            'mobile': assign_arg(line[int(config['participant_field_map']['mobile'])]),
            'gender': assign_arg(line[int(config['participant_field_map']['gender'])]),
            'location': assign_arg(line[int(config['participant_field_map']['location'])]),
            'org_names': config['participant_field_map']['org_names'],
            'org_0': assign_arg(line[int(config['participant_field_map']['org_fields'][0])]), 
            'org_1': assign_arg(line[int(config['participant_field_map']['org_fields'][1])]),
            'org_2': assign_arg(line[int(config['participant_field_map']['org_fields'][2])]),
            'org_3': assign_arg(line[int(config['participant_field_map']['org_fields'][3])]),
            'org_4': assign_arg(line[int(config['participant_field_map']['org_fields'][4])]),
            'org_5': assign_arg(line[int(config['participant_field_map']['org_fields'][5])]),
            'org_6': assign_arg(line[int(config['participant_field_map']['org_fields'][6])]),
            'org_7': assign_arg(line[int(config['participant_field_map']['org_fields'][7])]),
            'org_8': assign_arg(line[int(config['participant_field_map']['org_fields'][8])]),
            'org_9': assign_arg(line[int(config['participant_field_map']['org_fields'][9])]),
            'comment': 'imported',
            'is_active': True,
            'import_date': date_object.strftime("%Y-%m-%d"),
            'ancestors': [],
            'ancestor_names':[],
            'programs': {}
        }
        org = doc['organisation']
        empc = doc['emp_code']
        print(org + ' -' + empc)

        for icol, col in enumerate(line):
            if icol in list(map(int,config['participant_field_map']['ancestor_emp_codes'])):
                if col != "#N/A":
                    doc['ancestors'].append(col)
            if icol in list(map(int,config['participant_field_map']['ancestor_names'])):
                if col != "#N/A":
                    doc['ancestor_names'].append(col)

        try:
            db.org_parties.update_one(
                {'emp_code': empc },
                {'$set': doc},
                upsert = True
            )
        except Exception as e:
            print (e)

    rownbr += 1

# close the pointer to that file
client.close()