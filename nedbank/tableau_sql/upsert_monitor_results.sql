-- used for Hollard
drop procedure upsert_monitor_results;

CREATE PROCEDURE upsert_monitor_results
(
    IN p_project            VARCHAR(255),
	IN p_widget_id          CHAR(24),
    IN p_uuid               VARCHAR(255),
    IN p_dp_id              CHAR(24),
    IN p_wp_id              CHAR(24),
    IN p_full_name          VARCHAR(255),
	IN p_email 			    VARCHAR(255),
	IN p_mobile 		    VARCHAR(255),
	IN p_gid				VARCHAR(255),
	IN p_emp_code 		    VARCHAR(255),
	IN p_display_name       VARCHAR(255),
	IN p_nbr_gates	        INT,
	IN p_completion_pct     INT,
	IN p_nbr_questions_correct INT,
	IN p_correct_pct        INT,
	IN p_engagement_seconds FLOAT,
	IN p_gate_seconds       FLOAT,
    IN p_face_time_secs     FLOAT,
	IN p_start              VARCHAR(255),
	IN p_end                VARCHAR(255),
	IN p_lock_gate          VARCHAR(255),
	IN p_video_seconds      FLOAT,
	IN p_video_pct          FLOAT,
	IN p_channels           VARCHAR(255),
    IN p_tot_gates          INT,
    IN p_nbr_questions      INT,
    OUT msg                 VARCHAR(255)
)
sp: BEGIN
    DECLARE v_pr_count        INT;
    DECLARE v_comment         VARCHAR(255);

    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
        --ROLLBACK;
        GET DIAGNOSTICS CONDITION 1
    		@p2 = MESSAGE_TEXT;
    		SELECT @p2 into msg;
        --SELECT CONCAT('An error has occurred, operation rolled back and the stored procedure was terminated for ', p_full_name, ) INTO msg;
    END;


    --select CONCAT('upsert_participant_results, id: ', convert(v_part_id, CHAR));

    SELECT COUNT(*) INTO v_pr_count FROM monitor_results
    WHERE widget_id = p_widget_id
    AND   device_profile_id = p_dp_id
    AND   widget_profile_id = p_wp_id;

    select v_pr_count;

    IF v_pr_count > 0 THEN
        UPDATE monitor_results 
        SET     project = p_project,
                uuid = p_uuid,
                full_name = p_full_name,
                email = p_email,
                mobile = p_mobile,
                id_number = p_gid,
                employee_code = p_emp_code,
                display_name = p_display_name,
                nbr_gates = greatest(p_nbr_gates,nbr_gates),
                nbr_correct = greatest(p_nbr_questions_correct,nbr_correct),
                pct_complete = greatest(p_completion_pct,pct_complete),
                pct_correct = greatest(p_correct_pct, pct_correct),
                engagement_seconds = greatest(p_engagement_seconds, engagement_seconds),
                gate_seconds = greatest(p_gate_seconds,gate_seconds),
                face_time_seconds = greatest(p_face_time_secs, face_time_seconds),
                start = p_start,
                end = p_end,
                lock_gate = p_lock_gate,
                video_seconds = greatest(p_video_seconds, video_seconds),
                video_pct = greatest(p_video_pct, video_pct),
                channels_text = p_channels,
                total_nbr_gates = greatest(p_tot_gates, total_nbr_gates),
                total_nbr_questions = greatest(p_nbr_questions, total_nbr_questions),
                import_date = NOW(),
                nbr_repeats = nbr_repeats + 1
        WHERE   device_profile_id = p_dp_id
        AND     widget_profile_id = p_wp_id
        AND     widget_id = p_widget_id;
        select CONCAT('device_profile_id: ', p_dp_id, ', widget_profile_id: ', p_wp_id, ', widget_id: ', p_widget_id, ' updated.') INTO msg;
    ELSE
        INSERT INTO monitor_results(
            project,
            widget_id,
            uuid,
            device_profile_id,
            widget_profile_id,
            full_name,
            email,
            mobile,
            id_number,
            employee_code,
            display_name,
            nbr_gates,
            nbr_correct,
            pct_complete,
            pct_correct,
            engagement_seconds,
            gate_seconds,
            face_time_seconds,
            start,
            end,
            lock_gate,
            video_seconds,  
            video_pct,
            channels_text,
            total_nbr_gates,
            total_nbr_questions,
            import_date,
            nbr_repeats
        )
        VALUES (
            p_project,
            p_widget_id,
            p_uuid,
            p_dp_id,
            p_wp_id,
            p_full_name,
            p_email,
            p_mobile,
            p_gid,
            p_emp_code,
            p_display_name,
            p_nbr_gates,
            p_nbr_questions_correct,
            p_completion_pct,
            p_correct_pct,
            p_engagement_seconds,
            p_gate_seconds,
            p_face_time_secs,
            p_start,
            p_end,
            p_lock_gate,
            p_video_seconds,
            p_video_pct,
            p_channels,
            p_tot_gates,
            p_nbr_questions,
            NOW(),
            1
        );
        select CONCAT('device_profile_id: ', p_dp_id, ', widget_profile_id: ', p_wp_id, ', widget_id: ', p_widget_id,' inserted.') INTO msg;
    END IF;

END;