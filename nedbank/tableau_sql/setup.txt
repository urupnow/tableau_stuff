ubuntu@54.77.159.201

ssh -i /home/stephan/AWSkeys/urup_eu_west_1.pem ubuntu@54.77.159.201

scp -i /home/stephan/AWSkeys/urup_eu_west_1.pem nedbamk_2020_02_17.zip ubuntu@54.77.159.201:/home/ubuntu/mysql/upload_files/nedbank

pip3 install requests

pip3 install mysql-connector-python

pip3 install configobj

mysql
root/q[3d/9DR}5]
URUP_Reports/"[*dwg8ZfB 

UPDATE mysql.user SET File_priv = 'Y' WHERE user='URUP_Reports' AND host='localhost';
sudo chmod -R a+rwx ~/mysql/upload_files

test query:
select participant_results.* from participant_results 
join participant on participant.id = participant_results.participant_id
join participant_program on participant_program.participant_id = participant.id
join journey on journey.group_1 = participant_program.program_name and journey.widget_id = participant_results.widget_id
where participant.emp_code = 205585
and participant_program.program_name = 'FICAA awareness training'

eg update:
UPDATE
    merged_results INNER JOIN journey USING (widget_id)
SET
    merged_results.tot_nbr_gates = journey.nbr_gates,
    merged_results.tot_nbr_questions = journey.nbr_questions

eg query:
select org_1, org_2, org_3, org_4, org_5, pgm_1, pgm_2, 
sum(case when correct_pct = 100 then 1 else 0 end) as passes,
sum(case when correct_pct < 100 then 1 else 0 end) as fails,
count(*) as attempts,
sum(case when nbr_gates >= req_gates_pass then 1 else 0 end) as journey_completed,
sum(case when nbr_gates < req_gates_pass then 1 else 0 end) as incomplete_journey 
from merged_results
group by org_1, org_2, org_3, org_4, org_5, pgm_1, pgm_2
INTO OUTFILE '~/mysql/upload_files/summary.csv'
FIELDS TERMINATED BY ';'
ENCLOSED BY '"'
LINES TERMINATED BY '\n';


process steps
1) design and create database
2) extract list of widget_ids involved 
3) create/get csv files for participants, programs, participants_per_program
4) run imports in sequence:
    4.1 python nedbank_journeys_2_mysql.py          journeys -> db                  partial journey table from API
    4.2 python nedbank_program_csv_2_mysql.py       program_csv -> db               completes journey table from csv file
    4.3 python nedbank_participants_csv_2_mysql.py  participants -> db              participant table from csv file
    4.4 python nedbank_part_pgm_csv_2_mysql.py      participant_program -> db       participant_program table from csv file(s)
5) run import(s) of results - python nedbank_results_2_mysql.py             get_monitor_data_2 (API) -> participant_results table.
6) populate merged_results - delete merged_results, exec 'insert_merged_results'
