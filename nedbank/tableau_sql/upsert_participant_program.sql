drop procedure upsert_participant_program;

CREATE PROCEDURE upsert_participant_program
(
    IN p_id_field           VARCHAR(255),
    IN p_participant_id     VARCHAR(255),
    IN p_program_name       VARCHAR(255),
    IN p_requirement        VARCHAR(255),
    OUT msg                 VARCHAR(255)
)
BEGIN
    DECLARE v_part_id   INT;
    DECLARE v_cnt       INT;
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
        ROLLBACK;
        SELECT CONCAT('An error has occurred, operation rolledback and procedure was terminated for ', p_participant_id) INTO msg;
    END;

    IF (p_id_field = 'email') THEN
        SELECT id INTO v_part_id FROM participant 
        WHERE email = p_participant_id  
        AND is_active = true LIMIT 1;
    END IF;
    IF (p_id_field = 'mobile') THEN
        SELECT id INTO v_part_id FROM participant 
        WHERE mobile = p_participant_id 
        AND is_active = true LIMIT 1;
    END IF;
    IF (p_id_field = 'gid') THEN
        SELECT id INTO v_part_id FROM participant 
        WHERE gid = p_participant_id  
        AND is_active = true LIMIT 1;
    END IF;
    IF (p_id_field = 'emp_code') THEN
        SELECT id INTO v_part_id FROM participant 
        WHERE emp_code = p_participant_id 
        AND is_active = true LIMIT 1;
    END IF;
    IF (p_id_field = 'uuid') THEN
        SELECT id INTO v_part_id FROM participant 
        WHERE uuid = p_participant_id 
        AND is_active = true  LIMIT 1;
    END IF;
    IF (p_id_field = 'name') THEN
        SELECT id INTO v_part_id FROM participant 
        WHERE name = p_participant_id 
        AND is_active = true  LIMIT 1;
    END IF;

    IF ISNULL(v_part_id) THEN 
        SELECT -1 INTO v_part_id;
    END IF;

    SELECT COUNT(*) INTO v_cnt FROM participant_program
    WHERE participant_id = v_part_id
    AND program_name = p_program_name;

    SELECT v_part_id, v_cnt;

    IF v_cnt > 0 THEN
        UPDATE participant_program
        SET requirement = coalesce(p_requirement, requirement),
            import_date = NOW()
        WHERE participant_id = v_part_id
        AND program_name = p_program_name;
        commit;
        select CONCAT(v_part_id, ', ', p_program_name, ' updated.') INTO msg;
    ELSE
        insert into participant_program(
            participant_id, 
            program_name,
            requirement,
            import_date
        )
        values (
            v_part_id, 
            p_program_name, 
            p_requirement,
            NOW()
        );
        commit;
        select CONCAT(v_part_id, ', ', p_program_name, ' inserted.') INTO msg;
    END IF;
END;


call upsert_participant_program(
    'emp_code',
    '148689', 
    "Compliance training: Gifts, Entertainment & OBI",
    "Compulsory",
    @msg
);
select @msg;