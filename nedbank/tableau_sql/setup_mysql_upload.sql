// create db
CREATE DATABASE urup_imports

//create users
CREATE USER 'urup_import' IDENTIFIED BY 'w}vZMQ2.';
GRANT ALL PRIVILEGES ON urup_imports.* TO 'urup_import'@'%';


GRANT SELECT ON urup_imports.* TO 'URUP_Reports';

create table talentsolved_mercantile(
    formsid     VARCHAR(255),
    formstatusid    VARCHAR(255),
    uuid        VARCHAR(255),
    profile_id  VARCHAR(255),
    assigned_to VARCHAR(255),
    status      VARCHAR(255),
    statusdate  VARCHAR(255),
    accountname VARCHAR(255),
    contact     VARCHAR(255),
    position    VARCHAR(255),
    email       VARCHAR(255),
    phone       VARCHAR(255),
    cell        VARCHAR(255),
    vendor      VARCHAR(255),
    region      VARCHAR(255),
    sapno       VARCHAR(255),
    followup    VARCHAR(255),
    message_date    DATETIME,
    upload_date DATETIME,
    source      VARCHAR(255),
    client      VARCHAR(255),
    unexpected  VARCHAR(255)
);

ALTER TABLE talentsolved_mercantile ADD id INT PRIMARY KEY AUTO_INCREMENT;

create table failed_imports(
    id INT PRIMARY KEY AUTO_INCREMENT,
    message_date    DATETIME,
    upload_date DATETIME,
    source      VARCHAR(255),
    client      VARCHAR(255),
    payload     TEXT,
    comment     VARCHAR(255)
)

create table import_auth(
	id INT PRIMARY KEY auto_increment,
	source	VARCHAR(255),
	token	VARCHAR(255)
	);

insert into import_auth(source, token) values('mysolved', '4nAj>9"{YZK]MD#U');
insert into import_auth(source, token) values('talentsolved','4nAj>9"{YZK]MD#U');	
