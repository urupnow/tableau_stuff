drop function extract_number;

create function extract_number(
    p_str   VARCHAR(255),
    p_label VARCHAR(255) )
    RETURN int

    Begin
        declare v_pos   int;
        declare v_col   int;
        declare v_ph    int;
        declare v_sstr  varchar(255);
        
        set v_pos = INSTR(p_str, p_label);
        set v_sstr = SUBSTR(p_str, v_pos);

        set v_col = INSTR( v_sstr, ':');
        set v_ph = INSTR( v_sstr, '#');

        return CAST(SUBSTR(v_sstr, v_col + 1, v_ph - (v_col + 1)) AS UNSIGNED);

    End;
    );