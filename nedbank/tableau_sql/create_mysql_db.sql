ALTER TABLE journey_type DROP FOREIGN KEY `fk_jjt`;

drop table participant_results;
drop table participant;
drop table journey;
drop table journey_type;

create table journey_type (
	id INT NOT NULL PRIMARY KEY,
	name VARCHAR(255) NOT NULL
    );
    
create table journey (
	widget_id  CHAR(24) 	NOT NULL PRIMARY KEY,
	name 			VARCHAR(255) NOT NULL,
	start_dt 		DATETIME,
	end_dt 			DATETIME,
	type_id 		INT,
	nbr_gates 		INT,
	nbr_questions 	INT,
	repeatable 		BOOLEAN,
	region_code 	VARCHAR(255),
	completion_pass_pct	INT,
	correct_pass_pct 	INT,
	program 	VARCHAR(255),
	group_1		VARCHAR(255),
	group_2		VARCHAR(255),
	group_3		VARCHAR(255),
	group_4		VARCHAR(255),
	group_5		VARCHAR(255)
	);

create table participant (
    id  			INT AUTO_INCREMENT PRIMARY KEY,
	name 			VARCHAR(255) NOT NULL,
	email 			VARCHAR(255),
	mobile 			VARCHAR(255),
	gid				VARCHAR(255),
	emp_code 		VARCHAR(255),
	uuid			VARCHAR(255),
	age				INT,
	gender			CHAR(1),
	location		VARCHAR(255),
	org_structure	VARCHAR(255),
	program			VARCHAR(255),
	group_1			VARCHAR(255),
	group_2			VARCHAR(255),
	group_3			VARCHAR(255),
	group_4			VARCHAR(255),
	group_5			VARCHAR(255),
	group_6			VARCHAR(255),
	group_7			VARCHAR(255),
	group_8			VARCHAR(255),
	group_9			VARCHAR(255),
	is_active		BOOLEAN,
	comment			VARCHAR(255),
	import_date		DATE
	);

create table participant_results (
    participant_id INT,
	widget_id 	CHAR(24) NOT NULL,
	uuid		VARCHAR(255),
	dp_id		CHAR(24),
	wp_id		CHAR(24),
	full_name	VARCHAR(255),
	email		VARCHAR(255),
	mobile		VARCHAR(255),
	gid			VARCHAR(255),
	emp_code	VARCHAR(255),
	display_name VARCHAR(255),
	nbr_gates	INT,
	completion_pct INT,
	nbr_questions_correct INT,
	correct_pct INT,
	engagement_seconds INT,
	gate_seconds INT,
	start_dt VARCHAR(255),
	end_dt VARCHAR(255),
	lock_gate VARCHAR(255),
	video_seconds INT,
	video_pct INT,
	channels VARCHAR(255),
	import_date DATE,
	nbr_repeats INT,
    PRIMARY KEY(participant_id, widget_id)
	);

-- spreadsheet sheets: <something> required (6 sheets)
create table participant_program (
	program_name		VARCHAR(255)	NOT NULL,
	participant_id		VARCHAR(255)	NOT NULL,
	requirement			VARCHAR(255),
	import_date 		DATE,
	PRIMARY KEY(participant_id, program_name)
)


create view non_starters as
select participant.*, participant_program.program_name, participant_program.requirement  
from participant
join participant_program on participant.id = participant_program.participant_id
where id not in (select distinct participant_id from participant_results)
and participant.comment = 'Imported'
and participant.is_active = true


create view view_results as
select 	journey.widget_id as journey_id,
	journey.name as journey_name,
	journey.start_dt as journey_start_date,
	journey.end_dt as journey_end_date,
	journey.type_id as journey_type,
	journey.repeatable as journey_is_repeatable,
	journey.region_code,
	journey.completion_pass_pct as journey_gates,
	journey.correct_pass_pct as journey_questions,
	journey.group_1 as pgm_1,
	journey.group_2 as pgm_2,
	journey.group_3 as pgm_3,
	journey.group_4 as pgm_4,
	journey.group_5 as pgm_5,
	participant_program.requirement,
    participant.id as participant_id,
	participant.name as participant_name,
	participant.email,
	participant.mobile,
	participant.gid as id_nbr,
	participant.emp_code,
	participant.age,
	participant.gender,
	participant.location,
	participant.group_1 as org_1,
	participant.group_2 as org_2,
	participant.group_3 as org_3,
	participant.group_4 as org_4,
	participant.group_5 as org_5,
	participant.group_6 as org_6,
	participant.group_7 as org_7,
	participant.group_8 as org_8,
	participant.group_9 as org_9,
	participant_results.uuid,
	participant_results.dp_id,
	participant_results.wp_id,
	participant_results.display_name,
	participant_results.nbr_gates,
	participant_results.completion_pct,
	participant_results.nbr_questions_correct,
	participant_results.correct_pct,
	participant_results.engagement_seconds,
	participant_results.gate_seconds,
	participant_results.start_dt,
	participant_results.end_dt,
	participant_results.lock_gate,
	participant_results.video_seconds,
	participant_results.video_pct,
	participant_results.channels,
	participant_results.nbr_repeats,
	participant_results.gate_details,
	coalesce(participant_results.import_date,str_to_date('20191201','%Y%m%d')) as import_date
from participant 
join participant_program on participant_program.participant_id = participant.id
join participant_results on participant.id = participant_results.participant_id
join journey on journey.group_1 = participant_program.program_name and journey.widget_id = participant_results.widget_id
where participant.is_active = true
and   participant.comment = 'Imported';

create table results_import_log(
	doc	TEXT,
	msg  VARCHAR(255),
	imp_date DATE);


    ALTER TABLE journey ADD CONSTRAINT fk_jjt FOREIGN KEY (type_id) REFERENCES journey_type(id);
    ALTER TABLE participant_results ADD CONSTRAINT fk_prp FOREIGN KEY (participant_id) REFERENCES participant(id);
    ALTER TABLE participant_results ADD CONSTRAINT fk_prj FOREIGN KEY (widget_id) REFERENCES journey(widget_id);
    ALTER TABLE program_journey ADD CONSTRAINT fk_psps FOREIGN KEY (program_id) REFERENCES program(id);
    ALTER TABLE program_journey ADD CONSTRAINT fk_psj FOREIGN KEY (widget_id) REFERENCES journey(widget_id);
	ALTER TABLE participant_program ADD CONSTRAINT fk_ppp FOREIGN KEY (participant_id) REFERENCES participant(id);
	ALTER TABLE participant_program ADD CONSTRAINT fk_ppg FOREIGN KEY (program_id) REFERENCES program(id);

	create index part_div_idx on participant(group_1)

	create index jny_pgm_idx on journey(group_1)