insert into journey_type(id, name)
values (0, 'default')

insert into journey_type(id, name)
values (3, 'cluedup')

drop procedure upsert_journey;

DELIMITER //
CREATE PROCEDURE upsert_journey
(
    IN p_widget_id VARCHAR(24),
    IN p_name     VARCHAR(255),
    IN p_start    VARCHAR(255),
    IN p_end      VARCHAR(255),
    IN p_type_id  INT,
    IN p_nbr_gates    INT,
    IN p_nbr_questions    INT,
    IN p_repeatable   BOOLEAN,
    IN p_region_code  VARCHAR(255),
    IN p_completion_pct INT,
    IN p_correct_pct    INT,
    IN p_group_1        VARCHAR(255),
    IN p_group_2        VARCHAR(255),
    IN p_group_3        VARCHAR(255),
    IN p_group_4        VARCHAR(255),
    IN p_group_5        VARCHAR(255),
    IN p_program        VARCHAR(255),
    OUT msg             VARCHAR(255)
)
BEGIN
    DECLARE v_w_count   INT;
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
        ROLLBACK;
        SELECT CONCAT('An error has occurred, operation rollbacked and the stored procedure was terminated for ', p_name) INTO msg;
    END;

    SELECT COUNT(*) INTO v_w_count FROM journey
    WHERE widget_id = p_widget_id;

    IF v_w_count > 0 THEN
        UPDATE journey
        SET name = coalesce(p_name, name),
            start_dt = coalesce(STR_TO_DATE(p_start,'%Y-%m-%dT%T.%fZ'), start_dt),
            end_dt = coalesce(STR_TO_DATE(p_end,'%Y-%m-%dT%T.%fZ'), end_dt),
            type_id = coalesce(p_type_id,type_id),
            nbr_gates = coalesce(p_nbr_gates,nbr_gates),
            nbr_questions = coalesce(p_nbr_questions,nbr_questions),
            repeatable = coalesce(p_repeatable,repeatable),
            region_code = coalesce(p_region_code,region_code),
            completion_pass_pct = coalesce(p_completion_pct,completion_pass_pct),
            correct_pass_pct = coalesce(p_correct_pct,correct_pass_pct),
            group_1 = coalesce(p_group_1,group_1),
            group_2 = coalesce(p_group_2,group_2),
            group_3 = coalesce(p_group_3,group_3),
            group_4 = coalesce(p_group_4,group_4),
            group_5 = coalesce(p_group_5,group_5),
            program = coalesce(p_program,program)
        WHERE widget_id = p_widget_id;
        select CONCAT(p_widget_id, ' updated.') INTO msg;
    ELSE
        insert into journey(widget_id, 
                        name, 
                        start_dt, 
                        end_dt,
                        type_id, 
                        nbr_gates, 
                        nbr_questions, 
                        repeatable, 
                        region_code, 
                        completion_pass_pct, 
                        correct_pass_pct, 
                        group_1, 
                        group_2, 
                        group_3, 
                        group_4, 
                        group_5,
                        program
                    )
        values (p_widget_id, 
            p_name, 
            STR_TO_DATE(p_start,'%Y-%m-%dT%T.%fZ'), 
            STR_TO_DATE(p_end,'%Y-%m-%dT%T.%fZ'), 
            p_type_id, 
            p_nbr_gates, 
            p_nbr_questions, 
            p_repeatable, 
            p_region_code,
            p_completion_pct,
            p_correct_pct,
            p_group_1,
            p_group_2,
            p_group_3,
            p_group_4,
            p_group_5,
            p_program
            );
        select CONCAT(p_widget_id, ' inserted.') INTO msg;
    END IF;
END;
 //
DELIMITER ;

insert into journey(widget_id, name, start, end, nbr_gates, nbr_questions, type_id, repeatable, region_code)
 values('5dd2fe1c8119fa4b1b26f531','Culture Survey', '2019-11-19T06:10:00.000Z', '2019-11-29T22:01:00.000Z', 0, 17, 0, false, 'produk');

 call upsert_journey('5dd2fe1c8119fa4b1b26f531','Culture Survey', '2019-11-19T06:10:00.111Z', '2019-11-29T22:01:00.000Z', 17, 0, false, 'produk', 100, 0, 'FICAA awareness training', '1_Module 9',null, null, null, '{"program":"FICAA awareness training", "module":"1_Module 9"}', @msg);
 select @msg;
