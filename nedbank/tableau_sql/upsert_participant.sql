drop procedure upsert_participant;

CREATE PROCEDURE upsert_participant
(
    IN p_id_field       VARCHAR(255),
    IN p_name           VARCHAR(255),
    IN p_email          VARCHAR(255),
    IN p_mobile         VARCHAR(255),
    IN p_gid            VARCHAR(255),
    IN p_emp_code       VARCHAR(255),
    IN p_uuid           VARCHAR(255),
	IN p_age			INT,
	IN p_gender			CHAR(1),
	IN p_location		VARCHAR(255),
	IN p_group_1		VARCHAR(255),
	IN p_group_2		VARCHAR(255),
	IN p_group_3		VARCHAR(255),
	IN p_group_4		VARCHAR(255),
	IN p_group_5		VARCHAR(255),
	IN p_group_6		VARCHAR(255),
	IN p_group_7		VARCHAR(255),
	IN p_group_8		VARCHAR(255),
	IN p_group_9		VARCHAR(255),
    IN p_org_structure  VARCHAR(255),
    IN p_program        VARCHAR(255),
    IN p_comment        VARCHAR(255),
    IN p_is_active      BOOLEAN,
    IN p_overwrite      BOOLEAN,
    OUT msg             VARCHAR(255)
)
sp: BEGIN
        DECLARE     v_part_id    INT;

        DECLARE EXIT HANDLER FOR SQLEXCEPTION
        BEGIN
            ROLLBACK;
            SELECT CONCAT('An error has occurred, operation rollbacked and the stored procedure was terminated for ', p_name) INTO msg;
        END;

        IF (p_id_field = 'email') THEN
            SELECT id INTO v_part_id FROM participant 
            WHERE email = p_email LIMIT 1;
        END IF;
        IF (p_id_field = 'mobile') THEN
            SELECT id INTO v_part_id FROM participant 
            WHERE mobile = p_mobile  LIMIT 1;
        END IF;
        IF (p_id_field = 'gid') THEN
            SELECT id INTO v_part_id FROM participant 
            WHERE gid = p_gid  LIMIT 1;
        END IF;
        IF (p_id_field = 'emp_code') THEN
            SELECT id INTO v_part_id FROM participant 
            WHERE emp_code = p_emp_code  LIMIT 1;
        END IF;
        IF (p_id_field = 'uuid') THEN
            SELECT id INTO v_part_id FROM participant 
            WHERE uuid = p_uuid  LIMIT 1;
        END IF;
        IF (p_id_field = 'name') THEN
            SELECT id INTO v_part_id FROM participant 
            WHERE name = p_name  LIMIT 1;
        END IF;

        IF ISNULL(v_part_id) THEN 
            SELECT -1 INTO v_part_id;
        END IF;
        select v_part_id;

        IF v_part_id > 0 THEN

            IF p_overwrite THEN
                UPDATE participant
                SET name = p_name,
                    email = p_email,
                    mobile = p_mobile,
                    gid = p_gid,
                    emp_code = p_emp_code,
                    uuid = p_uuid,
                    age = p_age,
                    gender = p_gender,
                    location = p_location,
                    group_1 = p_group_1,
                    group_2 = p_group_2,
                    group_3 = p_group_3,
                    group_4 = p_group_4,
                    group_5 = p_group_5,
                    group_6 = p_group_6,
                    group_7 = p_group_7,
                    group_8 = p_group_8,
                    group_9 = p_group_9,
                    org_structure = p_org_structure,
                    program = p_program,
                    is_active = p_is_active,
                    comment = p_comment,
                    import_date = NOW()
                WHERE id = v_part_id;
            ELSE

                -- check inputs
                if p_name IN ('INR','ABU', 'NSR', '') THEN select NULL into p_name; end if;
                if p_email IN ('INR','ABU', 'NSR', '') THEN select NULL into p_email; end if;
                if p_mobile IN ('INR','ABU', 'NSR', '') THEN select NULL into p_mobile; end if;
                if p_gid IN ('INR','ABU', 'NSR', '') THEN select NULL into p_gid; end if;
                if p_emp_code IN ('INR','ABU', 'NSR', '') THEN select NULL into p_emp_code; end if;
                if p_uuid IN ('INR','ABU', 'NSR', '') THEN select NULL into p_uuid; end if;
                if p_age IN ('INR','ABU', 'NSR', '') THEN select NULL into p_age; end if;
                if p_gender IN ('INR','ABU', 'NSR', '') THEN select NULL into p_gender; end if;
                if p_location IN ('INR','ABU', 'NSR', '') THEN select NULL into p_location; end if;
                if p_group_1 IN ('INR','ABU', 'NSR', '') THEN select NULL into p_group_1; end if;
                if p_group_2 IN ('INR','ABU', 'NSR', '') THEN select NULL into p_group_2; end if;
                if p_group_3 IN ('INR','ABU', 'NSR', '') THEN select NULL into p_group_3; end if;
                if p_group_4 IN ('INR','ABU', 'NSR', '') THEN select NULL into p_group_4; end if;
                if p_group_5 IN ('INR','ABU', 'NSR', '') THEN select NULL into p_group_5; end if;
                if p_group_6 IN ('INR','ABU', 'NSR', '') THEN select NULL into p_group_6; end if;
                if p_group_7 IN ('INR','ABU', 'NSR', '') THEN select NULL into p_group_7; end if;
                if p_group_8 IN ('INR','ABU', 'NSR', '') THEN select NULL into p_group_8; end if;
                if p_group_9 IN ('INR','ABU', 'NSR', '') THEN select NULL into p_group_9; end if;
                if p_org_structure IN ('INR','ABU', 'NSR', '') THEN select NULL into p_org_structure; end if;
                if p_program IN ('INR','ABU', 'NSR', '') THEN select NULL into p_program; end if;
                if p_is_active IN ('INR','ABU', 'NSR', '') THEN select NULL into p_is_active; end if;
                if p_comment IN ('INR','ABU', 'NSR', '') THEN select NULL into p_comment; end if;

                UPDATE participant
                SET name = coalesce(p_name, name),
                    email = coalesce(p_email, email),
                    mobile = coalesce(p_mobile, mobile),
                    gid = coalesce(p_gid, gid),
                    emp_code = coalesce(p_emp_code, emp_code),
                    uuid = coalesce(p_uuid, uuid),
                    age = coalesce(p_age, age),
                    gender = coalesce(p_gender, gender),
                    location = coalesce(p_location, location),
                    group_1 = coalesce(p_group_1, group_1),
                    group_2 = coalesce(p_group_2, group_2),
                    group_3 = coalesce(p_group_3, group_3),
                    group_4 = coalesce(p_group_4, group_4),
                    group_5 = coalesce(p_group_5, group_5),
                    group_6 = coalesce(p_group_6, group_6),
                    group_7 = coalesce(p_group_7, group_7),
                    group_8 = coalesce(p_group_8, group_8),
                    group_9 = coalesce(p_group_9, group_9),
                    org_structure = coalesce(p_org_structure, org_structure),
                    program = coalesce(p_program, program),
                    is_active = coalesce(p_is_active, is_active),
                    comment = coalesce(p_comment, comment),
                    import_date = NOW()
                WHERE id = v_part_id;
            END IF;

            select CONCAT(CONVERT(v_part_id, CHAR), ' updated.') INTO msg;
        ELSE
            INSERT INTO participant(    name, 
                                        email, 
                                        mobile, 
                                        gid, 
                                        emp_code, 
                                        uuid, 
                                        age, 
                                        gender, 
                                        location, 
                                        group_1, 
                                        group_2, 
                                        group_3, 
                                        group_4, 
                                        group_5, 
                                        group_6, 
                                        group_7, 
                                        group_8, 
                                        group_9,
                                        org_structure,
                                        program,
                                        is_active,
                                        comment,
                                        import_date )
            VALUES (p_name, 
                    p_email, 
                    p_mobile, 
                    p_gid, 
                    p_emp_code, 
                    p_uuid, 
                    p_age, 
                    p_gender, 
                    p_location, 
                    p_group_1, 
                    p_group_2, 
                    p_group_3, 
                    p_group_4, 
                    p_group_5, 
                    p_group_6, 
                    p_group_7, 
                    p_group_8, 
                    p_group_9,
                    p_org_structure,
                    p_program,
                    p_is_active,
                    p_comment,
                    NOW() );

            select CONCAT(CONVERT(LAST_INSERT_ID(), CHAR), ' inserted.') INTO msg;
        END IF;
END;

call upsert_participant('email', 'Duane Vermeulen', 'dv@gmail.com', '+27 84 1238765', '8002265501008', '123dverm', '2636666twgutr8628618', 29, 'M', 'Gauteng', 'CSS', 'Currie', 'NIS', 'Breytenbach', 'Tranch 1', null, null, null, null,'{"Division":"CSS", "MANCO":"Currie", "SubBU": "NIS", "Manager": "Breytenbach", "Tranch": "Tranch1"}', '{"program":"FICAA awareness training", "module":"1_Module 9"}', true, true, 'test', @msg);
select @msg;
