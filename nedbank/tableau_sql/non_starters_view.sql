create view non_starters as
select participant.*, participant_program.program_name, participant_program.requirement  
from participant
join participant_program on participant.id = participant_program.participant_id
where id not in (select distinct participant_id from participant_results)
and participant.comment = 'Imported'
and participant.is_active = true