drop procedure upsert_participant_gate_results;

CREATE PROCEDURE upsert_participant_gate_results
(
    IN p_id_field           VARCHAR(255),
	IN p_widget_id          CHAR(24),
    IN p_uuid               VARCHAR(255),
    IN p_dp_id              CHAR(24),
    IN p_wp_id              CHAR(24),
    IN p_full_name          VARCHAR(255),
	IN p_email 			    VARCHAR(255),
	IN p_mobile 		    VARCHAR(255),
	IN p_gid				VARCHAR(255),
	IN p_emp_code 		    VARCHAR(255),
	IN p_display_name       VARCHAR(255),
	IN p_nbr_gates	        INT,
	IN p_completion_pct     INT,
	IN p_nbr_questions_correct INT,
	IN p_correct_pct        INT,
	IN p_engagement_seconds FLOAT,
	IN p_gate_seconds       FLOAT,
	IN p_start              VARCHAR(255),
	IN p_end                VARCHAR(255),
	IN p_lock_gate          VARCHAR(255),
	IN p_video_seconds      FLOAT,
	IN p_video_pct          FLOAT,
	IN p_channels           VARCHAR(255),
    IN p_gate_details       TEXT,
    IN p_upd_participant    BOOLEAN,
    OUT msg                 VARCHAR(255)
)
sp: BEGIN
    DECLARE v_part_id         INT;
    DECLARE v_pr_count        INT;
    DECLARE v_overwrite       BOOLEAN;
    DECLARE v_comment         VARCHAR(255);
    DECLARE v_score           INT;

    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
        --ROLLBACK;
        GET DIAGNOSTICS CONDITION 1
    		@p2 = MESSAGE_TEXT;
    		SELECT @p2 into msg;
        --SELECT CONCAT('An error has occurred, operation rolled back and the stored procedure was terminated for ', p_full_name, ) INTO msg;
    END;

    IF (p_id_field = 'email') THEN
        SELECT id INTO v_part_id FROM participant 
        WHERE UPPER(email) = UPPER(p_email) 
        AND is_active = true LIMIT 1;
    END IF;
    IF (p_id_field = 'mobile') THEN
        SELECT id INTO v_part_id FROM participant 
        WHERE mobile = p_mobile  
        AND is_active = true LIMIT 1;
    END IF;
    IF (p_id_field = 'gid') THEN
        SELECT id INTO v_part_id FROM participant 
        WHERE gid = p_gid  
        AND is_active = true LIMIT 1;
    END IF;
    IF (p_id_field = 'emp_code') THEN
        SELECT id INTO v_part_id FROM participant 
        WHERE UPPER(emp_code) = UPPER(p_emp_code)  
        AND is_active = true LIMIT 1;
    END IF;
    IF (p_id_field = 'uuid') THEN
        SELECT id INTO v_part_id FROM participant 
        WHERE uuid = p_uuid  
        AND is_active = true LIMIT 1;
    END IF;
    IF (p_id_field = 'name') THEN
        SELECT id INTO v_part_id FROM participant 
        WHERE UPPER(name) = UPPER(p_full_name)  
        AND is_active = true LIMIT 1;
    END IF;

    IF ISNULL(v_part_id) THEN
        SELECT CONCAT('Inserted from results of Journey: ', p_widget_id) INTO v_comment;
        SELECT true INTO v_overwrite;
    ELSE
        SELECT CONCAT('Updated from results of Journey: ', p_widget_id) INTO v_comment;
        SELECT false INTO v_overwrite;
    END IF;

    IF p_upd_participant AND ISNULL(v_part_id) THEN 
        call upsert_participant(
            p_id_field,
            p_full_name,
            p_email,
            p_mobile,
            p_gid,
            p_emp_code,
            p_uuid,
            NULL,   --age
            NULL,   --gender
            NULL,   --location
            NULL,   --group1
            NULL,   --group2
            NULL,   --group3
            NULL,   --group4
            NULL,   --group5
            NULL,   --group6
            NULL,   --group7
            NULL,   --group8
            NULL,   --group9
            NULL,   --org_structure
            NULL,   --program
            v_comment,      --comment
            true,           --is_active
            v_overwrite,    --overwrite
            msg
        );
        select cast(replace(replace(msg, ' inserted.', ''), ' updated.', '') as UNSIGNED) into v_part_id;
    END IF; 

    --select CONCAT('upsert_participant_results, id: ', convert(v_part_id, CHAR));

    IF v_part_id > 0 THEN 
        SELECT COUNT(*) INTO v_pr_count FROM participant_results
        WHERE widget_id = p_widget_id
        AND   participant_id = v_part_id;
    END IF;

    select v_pr_count;

    IF v_pr_count > 0 THEN

        SELECT gate_seconds INTO v_score FROM participant_results
        WHERE widget_id = p_widget_id
        AND   participant_id = v_part_id;

        IF v_score + 3 >= p_gate_seconds THEN

            UPDATE participant_results 
            SET     uuid = p_uuid,
                    dp_id = p_dp_id,
                    wp_id = p_wp_id,
                    full_name = p_full_name,
                    email = p_email,
                    mobile = p_mobile,
                    gid = p_gid,
                    emp_code = p_emp_code,
                    display_name = p_display_name,
                    nbr_gates = greatest(p_nbr_gates,nbr_gates),
                    nbr_questions_correct = greatest(p_nbr_questions_correct,nbr_questions_correct),
                    completion_pct = greatest(p_completion_pct,completion_pct),
                    correct_pct = greatest(p_correct_pct, correct_pct),
                    engagement_seconds = greatest(p_engagement_seconds, engagement_seconds),
                    gate_seconds = greatest(p_gate_seconds,gate_seconds),
                    start_dt = p_start,
                    end_dt = p_end,
                    lock_gate = p_lock_gate,
                    video_seconds = greatest(p_video_seconds, video_seconds),
                    video_pct = greatest(p_video_pct, video_pct),
                    channels = p_channels,
                    gate_details = p_gate_details,
                    import_date = NOW(),
                    nbr_repeats = nbr_repeats + 1
            WHERE   participant_id = v_part_id
            AND     widget_id = p_widget_id;
            select CONCAT(CONVERT(v_part_id, CHAR), ', ', p_widget_id, ' updated.') INTO msg;

        END IF;
    ELSE
        INSERT INTO participant_results(
            participant_id, widget_id,
            uuid,
            dp_id,
            wp_id,
            full_name,
            email,
            mobile,
            gid,
            emp_code,
            display_name,
            nbr_gates,
            nbr_questions_correct,
            completion_pct,
            correct_pct,
            engagement_seconds,
            gate_seconds,
            start_dt,
            end_dt,
            lock_gate,
            video_seconds,  
            video_pct,
            channels,
            gate_details,
            import_date,
            nbr_repeats
        )
        VALUES (
            v_part_id, p_widget_id,
            p_uuid,
            p_dp_id,
            p_wp_id,
            p_full_name,
            p_email,
            p_mobile,
            p_gid,
            p_emp_code,
            p_display_name,
            p_nbr_gates,
            p_nbr_questions_correct,
            p_completion_pct,
            p_correct_pct,
            p_engagement_seconds,
            p_gate_seconds,
            p_start,
            p_end,
            p_lock_gate,
            p_video_seconds,
            p_video_pct,
            p_channels,
            p_gate_details,
            NOW(),
            1
        );
        select CONCAT(CONVERT(v_part_id, CHAR), ', ', p_widget_id, ' inserted.') INTO msg;
    END IF;

END;

call upsert_participant_gate_results('email',
    '5dd2fe1c8119fa4b1b26f531',
    '2636666twgutr8628618',
    '5dd2fe1c8119fa4b1b26f531',
    '5dd2fe1c8119fa4b1b26f531',
    'Duane Vermeulen',
    'dv@abc.com',
    '+27 82 1234567',
    '8503045033099',
    'dv_1999',
    'dv', 
    17, 
    100, 
    0, 
    0, 
    0.5, 
    305.77, 
    '2019-07-16T14:39:53.709Z', 
    '2019-10-15T11:01:57.498Z', 
    'N/A', 
    0, 
    0, 
    'N/A',
    '',
    true,
    @msg);

select @msg;
