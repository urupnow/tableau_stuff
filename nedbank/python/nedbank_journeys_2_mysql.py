import requests
import json
import mysql.connector
from mysql.connector import Error
from mysql.connector import errorcode

from configobj import ConfigObj
config = ConfigObj('../config.ini')

#print(config["mysql"])

def jprint(obj):
    # create a formatted string of the Python JSON object
    text = json.dumps(obj, sort_keys=True, indent=4)
    print(text)

req_name = 'widget_details'
project = {
    "widget.name": 1,
    "widget.region_code": 1,
    "widget.specification.type": 1,
    "widget.specification.start_date": 1,
    "widget.specification.end_date": 1,
    "widget.specification.repeatable": 1,
    "widget.clued_up_specification.number_of_questions_from_pool": 1    
}
#args = '{%22widget_id_list%22:[%225cd96912c0fa0c08bb808a7a%22,%225cda7cecc0fa0c08b77c78bd%22,%225cda81b09367ec08b87c0179%22,%225cda7e1d9367ec0937d2169d%22,%225d510731601adc08c8cdead8%22,%225cda7f50c0fa0c08b77c78c0%22,%225cda7fc3c0fa0c08bf5b99fb%22,%225d6e27be601adc0951270496%22,%225cda808f9367ec093b14c116%22,%225cda80ee9367ec093b14c119%22,%225cda81469cf234092a4cec20%22,%225d4440a5601adc08cc1bf7e3%22,%225d493a5e601adc095126ff36%22,%225d493aabb593af08bc77797d%22,%225d493ae5b593af08b446ca12%22,%225d493aeb601adc08c8cdea1c%22,%225d493b0e601adc095126ff39%22,%225d493b4eb593af0943e891dc%22,%225cabb47a9367ec08c49bc75c%22,%225cabb5cac0fa0c08bf5b939f%22,%225d51128b601adc08c8cdeadc%22],%22platform%22:%22produk%22}'
#args = '{%22widget_id_list%22:[%225caba2599367ec0937d214a8%22,%225caba84fc0fa0c08bb806ec6%22,%225caba9429cf234092a4cea35%22,%225caba9afc0fa0c08bf5b939c%22,%225cabaa9c9367ec08b87be56e%22,%225cabaacc9cf23408b9f7b10a%22,%225cababf69367ec0937d214ab%22,%225cabac0f9367ec08c49bc753%22,%225cabad0a9cf23408b58d46f0%22,%225cabad1f9367ec08b87be571%22,%225cabade49367ec08b87be574%22,%225cabb1eac0fa0c08bb806ecc%22,%225cabb1efc0fa0c08bb806ecf%22,%225cabb2c79367ec08c49bc756%22,%225cabb2a19367ec093b14a545%22,%225cabb3919367ec093b14a548%22,%225cabb38c9367ec08c49bc759%22,%225cabb4809367ec08c49bc75f%22],%22platform%22:%22produk%22}'
#5db2ed637bab0278c0a1e191
#args = '{%22widget_id_list%22:[%225dfa01321ca4524b2c721721%22,%225dfa11e91359c658a318327a%22],%22platform%22:%22produk%22}'
args = '{%22widget_id_list%22:[%225c9c7ab90ff24e087640ffc7%22],%22platform%22:%22produk%22}'
parameters = {
    'request': req_name,
    'args': args
}
api_token = '9d558aa64e5db12e'
api_url_base = 'https://stephan_reports.urup.com/api/reports/v3'
headers = {'Content-Type': 'application/json',
           'Authorization': 'Bearer {0}'.format(api_token)}

req = '{0}?request={1}&args={2}'.format(api_url_base, req_name, args)
response = requests.get(req, headers=headers)

jprint(response.json())
#print (response.json()["count"])

data = response.json()["documents"]
out = ''
#exc stored proc
try:
    connection = mysql.connector.connect(host = config['mysql']['host'],
                                         port = config['mysql']['port'],
                                         database = config['mysql']['database'],
                                         user = config['mysql']['user'],
                                         password = config['mysql']['password'],
                                         autocommit = True )
    cursor = connection.cursor()

    for doc in data:
        nbr_questions = 0
        if "clued_up_specification" in doc["widget"]:
            nbr_questions = doc["widget"]["clued_up_specification"]["number_of_questions_from_pool"]
        
        spargs = [
            doc["_id"],
            doc["widget"]["name"],
            doc["widget"]["specification"]["start_date"],
            doc["widget"]["specification"]["end_date"],
            doc["widget"]["specification"]["type"],
            0,
            nbr_questions,
            doc["widget"]["specification"]["repeatable"],
            doc["widget"]["region_code"],
            0,
            0,
            None,   #group_1
            None,
            None,
            None,
            None,   #group_5
            None,    #program
            out
        ]
        cursor.callproc('upsert_journey', spargs)
        out = cursor.fetchone()
        print (out)

except mysql.connector.Error as error:
    print("Failed to execute stored procedure: {}".format(error))
finally:
    if (connection.is_connected()):
        cursor.close()
        connection.close()
        print("MySQL connection is closed")