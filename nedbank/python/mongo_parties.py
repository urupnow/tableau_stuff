from pymongo import MongoClient
from bson import ObjectId
import csv

#import os
#os.chdir(os.path.dirname(__file__))
from configobj import ConfigObj
config = ConfigObj("config.ini")
#print(config['files'])

import datetime
date_object = datetime.date.today()

# connect to MongoDB
uri = "mongodb://urupuser:Md5#M~{5cs#5zjnd@mongospreview.urup.com/urup:27017?authSource=admin"
#uri = "mongodb://urupuser:Md5#M~{5cs#5zjnd@34.245.121.178:27017?authSource=admin"

client = MongoClient(uri)
db=client.urup

# open the file for reading
with open(config['files']['participants']) as csvfile:
    data = list(csv.reader(csvfile, delimiter=';'))

   
def assign_arg(conf_item):
    try:
        if (len(conf_item) >= 0):
            return conf_item
        else: 
            return None
    except ValueError:
        return None


rownbr = 0
hdr = []
for line in data:
    # read a single line
    if rownbr == 0:
        hdr = line
        print(hdr)
    else:
        doc={
            'organisation': assign_arg(line[int(config['participant_field_map']['org'])]),
            'emp_code': assign_arg(line[int(config['participant_field_map']['emp_code'])]),
            'full_name': assign_arg(line[int(config['participant_field_map']['name'])]),
            'email': assign_arg(line[int(config['participant_field_map']['email'])]),
            'mobile': assign_arg(line[int(config['participant_field_map']['mobile'])]),
            'gender': assign_arg(line[int(config['participant_field_map']['gender'])]),
            'location': assign_arg(line[int(config['participant_field_map']['location'])]),
            'org_names': config['participant_field_map']['org_names'],
            'org_1': assign_arg(line[int(config['participant_field_map']['org_fields'][0])]), 
            'org_2': assign_arg(line[int(config['participant_field_map']['org_fields'][1])]),
            'org_3': assign_arg(line[int(config['participant_field_map']['org_fields'][2])]),
            'org_4': assign_arg(line[int(config['participant_field_map']['org_fields'][3])]),
            'org_5': assign_arg(line[int(config['participant_field_map']['org_fields'][4])]),
            'org_6': assign_arg(line[int(config['participant_field_map']['org_fields'][5])]),
            'org_7': assign_arg(line[int(config['participant_field_map']['org_fields'][6])]),
            'org_8': assign_arg(line[int(config['participant_field_map']['org_fields'][7])]),
            'org_9': assign_arg(line[int(config['participant_field_map']['org_fields'][8])]),
            'comment': 'imported',
            'is_active': True,
            'import_date': date_object.strftime("%Y-%m-%d"),
            'ancestors': [],
            'ancestor_names':[],
            'programs': {}
        }
        org = doc['organisation']
        empc = doc['emp_code']
        print(org + ' -' + empc)

        for icol, col in enumerate(line):
            if icol in list(map(int,config['participant_field_map']['ancestor_emp_codes'])):
                if col != "#N/A":
                    doc['ancestors'].append(col)
            if icol in list(map(int,config['participant_field_map']['ancestor_names'])):
                if col != "#N/A":
                    doc['ancestor_names'].append(col)

        db.org_parties.update_one(
            {'organisation': org, 'emp_code': empc },
            {'$set': doc},
            upsert = True
        )
    rownbr += 1

# close the pointer to that file
client.close()