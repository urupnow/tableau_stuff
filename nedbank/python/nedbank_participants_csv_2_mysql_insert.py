            # read csv file of personnel info, extract relevant data and exec 'upsert_participant' to populate participants.

import csv
import json

import mysql.connector
from mysql.connector import Error
from mysql.connector import errorcode

from configobj import ConfigObj
config = ConfigObj('config.ini')

with open('participants_nedbank.csv') as csvfile:
    data = list(csv.reader(csvfile, delimiter=';'))

print(config)

sql_ins = "INSERT INTO participant(name,email,mobile,gid,emp_code,uuid,age,gender,location,group_1,group_2,group_3,group_4,group_5,group_6,group_7,group_8,group_9,org_structure,program,is_active,comment) VALUES "

#exc stored proc
try:
    connection = mysql.connector.connect(host = config['mysql']['host'],
                                         port = config['mysql']['port'],
                                         database = config['mysql']['database'],
                                         user = config['mysql']['user'],
                                         password = config['mysql']['password'],
                                         autocommit = True )

    cursor = connection.cursor()
    out = ''
    args_str = ''
    for index, row in enumerate(data):
        if index > 0:
            org = {
                "Division": row[41],
                "MANCO": row[46],
                "SubBU": row[60],
                "Manager": row[30],
                "Tranch": row[63]
            }
            pgm = {}
            args = [
                row[4],     #name
                row[51],    #email
                row[52],    #mobile
                row[16],    #id
                row[2],     #emp_code
                '',       #uuid
                0,          #age
                row[55][0], #gender
                row[50],    #location
                row[41],    #division
                row[46],    #manco
                row[60],    #subbu
                row[30],    #manager
                row[63],    #tranch
                '',
                '',
                '',
                '',
                json.dumps(org),    #org
                json.dumps(pgm),    #program
                1,
                'Imported'     #comment       
            ]
            args_str  = '(' + str(args).strip('[]') + '),' + args_str
            if index % 10 == 0: 
                print (sql_ins + args_str + ';')
                cursor.execute(sql_ins + args_str + ';')
                args_str = ''


        #out = cursor.fetchone()
        #print out

except mysql.connector.Error as error:
    print("Failed to execute stored procedure: {}".format(error))
finally:
    if (connection.is_connected()):
        cursor.close()
        connection.close()
        print("MySQL connection is closed")

 