# read csv file of personnel info, extract relevant data and exec 'upsert_participant' to populate participants.

import csv
import json

import mysql.connector
from mysql.connector import Error
from mysql.connector import errorcode

from configobj import ConfigObj
config = ConfigObj('./config.ini')

def assign_arg(conf_item):
    try:
        if (len(conf_item) >= 0):
            return conf_item
        else: 
            return None
    except ValueError:
        return None

#print(config)

with open(config['files']['participants']) as csvfile:
    data = list(csv.reader(csvfile, delimiter=';'))

#exec stored proc
try:

    connection = mysql.connector.connect(host = config['mysql']['host'],
                                         port = config['mysql']['port'],
                                         database = config['mysql']['database'],
                                         user = config['mysql']['user'],
                                         password = config['mysql']['password'],
                                         autocommit = True )

    cursor = connection.cursor()
    out = ''

    #print(config['participant_field_map']['name'])
    cursor.execute("update participant set is_active = false")

    for row in data:
        org = {}

        for i in range(len(config['participant_field_map']['org_fields'])):
             org[config['participant_field_map']['org_names'][i]] = row[int(config['participant_field_map']['org_fields'][i])]
            
        pgm = {}
        #print(row[63])

        #specific to nedbank
        emp_code = assign_arg(row[int(config['participant_field_map']['emp_code'])])
        emp_code_n = ''.join([n for n in str(emp_code) if n.isdigit()])
        if len(emp_code_n) > 0:
            emp_code = str(int(emp_code_n))
            id_field = 'emp_code'
        else:
            id_field = 'email'


        args = [
            id_field,
            assign_arg(row[int(config['participant_field_map']['name'])]),
            assign_arg(row[int(config['participant_field_map']['email'])]),
            assign_arg(row[int(config['participant_field_map']['mobile'])]),
            assign_arg(row[int(config['participant_field_map']['gid'])]),
            emp_code,
            None,       #uuid
            #assign_arg(row[int(config['participant_field_map']['age'])]),
            None,
            assign_arg(row[int(config['participant_field_map']['gender'])])[0],
            assign_arg(row[int(config['participant_field_map']['location'])]),
            assign_arg(row[int(config['participant_field_map']['org_fields'][0])]),    #division
            assign_arg(row[int(config['participant_field_map']['org_fields'][1])]),    #manco
            assign_arg(row[int(config['participant_field_map']['org_fields'][2])]),    #subbu
            assign_arg(row[int(config['participant_field_map']['org_fields'][3])]),    #manager
            assign_arg(row[int(config['participant_field_map']['org_fields'][4])]),    #tranch
            # assign_arg(row[int(config['participant_field_map']['org_fields'][5])]),
            # assign_arg(row[int(config['participant_field_map']['org_fields'][6])]),
            # assign_arg(row[int(config['participant_field_map']['org_fields'][7])]),
            # assign_arg(row[int(config['participant_field_map']['org_fields'][8])]),
            None,
            None,
            None,
            None,
            json.dumps(org),    #org
            json.dumps(pgm),    #program
            'Imported',     #comment       
            True,         #is_active
            True,         #overwrite
            out
        ]
        #print(args)

        cursor.callproc('upsert_participant', args)
        out = cursor.fetchone()
        #print out

except mysql.connector.Error as error:
    print("Failed to execute stored procedure: {}".format(error))
finally:
    if (connection.is_connected()):
        cursor.close()
        connection.close()
        print("MySQL connection is closed")


#"Company Name";"RegCountry";"NB Emplyoee";"X";"Full Name"; (4)
# "Title";"Ethnic Origin";"Employment Status";"RSA Citizen";"RSA Status"; (9)
# "Challenge Indicator";"Previous Division";"Pay Grade";"Retirment Month";"Date of birth"; (14)
# "Hire date";"ID Number";"Position";"Position Name";"Gender"; (19)
# "Organizational Unit";"Organizational Unit Name";"Nationality";"Management Levels";"Master Cost Center"; (24)
# "Master Cost Center Name";"Employee Group";"Employee Subgroup";"Job";"Reporting to Employee number"; (29)
# "Manager Name";"Level 2 NB";"Level 2 Name";"Level 3 NB";"Level 3 Name";  (34_)
# "Level 4 NB";"Level 4 Name";"Level 5 NB";"Level 5 Name";"Level 6 NB";   (39)
# "Level 6 Name";"Division";"HR Head";"Exco Member";"Offset";  (44)
# "Offset # 1";"Manco";"Birthday Month";"Length Of Service";"Building";  (49)
# "RegProvince";"Email";"Cell Phone";"Ethnic Origin FN";"Management Levels";   (54)
# "Gen";"P/NP";"Division Match";"Prev Emp Group";"Emp Group Match";   (59)
# "Sub BU";"Cluster";"Age Generation";"Tranch"   (63)
