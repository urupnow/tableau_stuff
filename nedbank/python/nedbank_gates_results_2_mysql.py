import requests
import json
import mysql.connector
from mysql.connector import Error
from mysql.connector import errorcode

from configobj import ConfigObj
config = ConfigObj('/home/ubuntu/mysql/upload_files/config.ini')

def jprint(obj):
    # create a formatted string of the Python JSON object
    text = json.dumps(obj, sort_keys=True, indent=4)
    print(text)

req_name = 'get_monitor_data_3'

widget_ids = '%225dd2682ede18da1ea84f0b64%22'

args = '%7B%22widget_id%22:{id_list},%22extended_data%22:true,%22platform%22:%22produk%22,%22flatten%22:true,%22dates%22:[%222020-01-15 06:15:00%22,%222020-12-31%22]%7D'.format(id_list = widget_ids)
#jprint(args)

api_token = '9d558aa64e5db12e'
api_url_base = 'https://galdaganystage.urup.com/api/reports/v3'
headers = {'Content-Type': 'application/json',
           'Authorization': 'Bearer {0}'.format(api_token)}

import_log_sql = ''' INSERT INTO results_import_log(doc, msg, imp_date) values (%s, %s, NOW()); '''           

req = '{0}?request={1}&args={2}'.format(api_url_base, req_name, args)
#jprint(req)

response = requests.get(req, headers=headers)

#jprint(response.json())
print (response.json()["data"]["count"])

data = response.json()["data"]["profiles"]
out = ''
#exc stored proc
try:

    connection = mysql.connector.connect(host = config['mysql']['host'],
                                         port = config['mysql']['port'],
                                         database = config['mysql']['database'],
                                         user = config['mysql']['user'],
                                         password = config['mysql']['password'],
                                         autocommit = True )

    cursor = connection.cursor()

    for profile in data:

        emp_code = 'INR'
        if 'employee_code' in profile and profile['employee_code'] not in ('INR', 'ABU', 'NSU'):
            emp_code = ''.join([n for n in str(profile["employee_code"]) if n.isdigit()])
            if len(emp_code) > 0:
                emp_code = str(int(emp_code))
                id_field = 'emp_code'
            else:
                continue
        elif 'email' in profile and profile['email'] not in ('INR', 'ABU','NSU'):
            id_field = 'email'
        elif 'mobile' in profile and profile['mobile'] not in ('INR', 'ABU','NSU'):
            id_field = 'mobile'
        else:
            vals = (json.dumps(profile), 'No suitable id field found' )
            cursor.execute(import_log_sql, vals)
            #connection.commit()
            print('No suitable id field found')
            continue

        if len(profile) >= 34:
            spargs = [
                id_field,
                profile["widget_id"],
                profile["uuid"],
                profile["device_profile_id"],
                profile["widget_profile_id"],
                profile["full_name"],
                profile["email"],
                profile["mobile"],
                profile["government_identification_number"],
                emp_code,
                None,
                profile["nbr_gates"],
                profile["pct_complete"],
                profile["nbr_correct"],
                profile["pct_correct"],
                profile["engagement_seconds"],
                profile["gate_seconds"],
                profile["start"],
                profile["end"],
                profile["lock_gate"],
                profile["video_seconds"],
                profile["video_pct"],
                profile["channels_text"],
                profile["gate_details"],
                False,
                out
            ]
            #print (spargs)
            
            cursor.callproc('upsert_participant_gate_results', spargs)
            #out = cursor.fetchone()
            #print (out)
        else:
            vals = (json.dumps(profile), 'Some fields are missing' )
            cursor.execute(import_log_sql, vals)
           

        

except mysql.connector.Error as error:
    print("Failed to execute stored procedure: {}".format(error))
finally:
    if (connection.is_connected()):
        cursor.close()
        connection.close()
        print("MySQL connection is closed")
