import requests
import json
import mysql.connector
from mysql.connector import Error
from mysql.connector import errorcode

from configobj import ConfigObj
config = ConfigObj('/home/ubuntu/mysql/upload_files/config.ini')

def jprint(obj):
    # create a formatted string of the Python JSON object
    text = json.dumps(obj, sort_keys=True, indent=4)
    print(text)

req_name = 'get_monitor_data_2'

widget_ids = ''
#widget_ids += '%225cd96912c0fa0c08bb808a7a%22,%225cda7f50c0fa0c08b77c78c0%22,'
widget_ids += '%225cd96912c0fa0c08bb808a7a%22,%225cda7cecc0fa0c08b77c78bd%22,%225cda81b09367ec08b87c0179%22,'
widget_ids += '%225cda7e1d9367ec0937d2169d%22,%225d510731601adc08c8cdead8%22,%225cda7f50c0fa0c08b77c78c0%22,'
widget_ids += '%225cda7fc3c0fa0c08bf5b99fb%22,%225d6e27be601adc0951270496%22,%225cda808f9367ec093b14c116%22,'
widget_ids += '%225cda80ee9367ec093b14c119%22,%225cda81469cf234092a4cec20%22,%225d4440a5601adc08cc1bf7e3%22,'
widget_ids += '%225d493a5e601adc095126ff36%22,%225d493aabb593af08bc77797d%22,%225d493ae5b593af08b446ca12%22,'
widget_ids += '%225d493aeb601adc08c8cdea1c%22,%225d493b0e601adc095126ff39%22,%225d493b4eb593af0943e891dc%22,'
widget_ids += '%225db2ed637bab0278c0a1e191%22,%225db2fc52a347134ba7874987%22,%225d51128b601adc08c8cdeadc%22'
## 20 recs out of 20, 4 recs out of 4, 1643 recs out of 1719
#widget_ids += '%225dd2682ede18da1ea84f0b64%22'

#widget_ids += '%225cda7f50c0fa0c08b77c78c0%22'

# %225cabb47a9367ec08c49bc75c%22,%225cabb5cac0fa0c08bf5b939f%22'

#emails#widget_ids = '%225cabb47a9367ec08c49bc75c%22,%225cabb5cac0fa0c08bf5b939f%22'

#widget_ids = '%225caba2599367ec0937d214a8%22,%225caba84fc0fa0c08bb806ec6%22,%225caba9429cf234092a4cea35%22,%225caba9afc0fa0c08bf5b939c%22,%225cabaa9c9367ec08b87be56e%22,%225cabaacc9cf23408b9f7b10a%22,%225cababf69367ec0937d214ab%22,%225cabac0f9367ec08c49bc753%22,%225cabad0a9cf23408b58d46f0%22,%225cabad1f9367ec08b87be571%22,%225cabade49367ec08b87be574%22,%225cabb1eac0fa0c08bb806ecc%22,%225cabb1efc0fa0c08bb806ecf%22,%225cabb2c79367ec08c49bc756%22,%225cabb2a19367ec093b14a545%22,%225cabb3919367ec093b14a548%22,%225cabb38c9367ec08c49bc759%22,%225cabb4809367ec08c49bc75f%22'
#widget_ids = '%225c764a5c18ff37088f150f28%22,%225c766bbc18ff37088b2938e7%22,%225c766c7afcaab208902b076a%22,%225c766ce19ade94091ee6987e%22,%225c766d8f18ff37090cf0f7b9%22,%225c766e1118ff37090cf0f7bc%22,%225c766e6b18ff37088b2938ec%22,%225c766ed09ade940898301b27%22,%225c766f2c18ff37088f150f3d%22,%225c766f7918ff370893374db0%22,%225c766fc518ff37088f150f40%22,%225c94d4942dc3c3087527dd1e%22,%225c94d71f0ff24e087640ff4c%22,%225c94d8540ff24e087640ff4f%22,%225c94d9f52dc3c308fe978a85%22,%225c94db1a1eca7d0943165f1b%22,%225c94de850ff24e087e279827%22,%225c94e0431eca7d0943165f1e%22'

#widget_ids = '%225db2fc52a347134ba7874987%22,%225cabb47a9367ec08c49bc75c%22,%225c98e7630ff24e08729c269c%22'
#widget_ids = '%225cabb5cac0fa0c08bf5b939f%22'

args = '%7B%22widget_id_list%22:[{id_list}],%22extended_data%22:true,%22platform%22:%22produk%22,%22flatten%22:true,%22dates%22:[%222019-12-26 08:15:00%22,%222020-12-31%22]%7D'.format(id_list = widget_ids)
#jprint(args)

api_token = '9d558aa64e5db12e'
api_url_base = 'https://galdaganystage.urup.com/api/reports/v3'
headers = {'Content-Type': 'application/json',
           'Authorization': 'Bearer {0}'.format(api_token)}

import_log_sql = ''' INSERT INTO results_import_log(doc, msg, imp_date) values (%s, %s, NOW()); '''           

req = '{0}?request={1}&args={2}'.format(api_url_base, req_name, args)
#jprint(req)

response = requests.get(req, headers=headers)

#jprint(response.json())
print (response.json()["data"]["count"])

data = response.json()["data"]["profiles"]
out = ''
#exc stored proc
try:

    connection = mysql.connector.connect(host = config['mysql']['host'],
                                         port = config['mysql']['port'],
                                         database = config['mysql']['database'],
                                         user = config['mysql']['user'],
                                         password = config['mysql']['password'],
                                         autocommit = True )

    cursor = connection.cursor()

    for profile in data:

        emp_code = 'INR'
        if 'employee_code' in profile and profile['employee_code'] not in ('INR', 'ABU', 'NSU'):
            emp_code = ''.join([n for n in str(profile["employee_code"]) if n.isdigit()])
            if len(emp_code) > 0:
                emp_code = str(int(emp_code))
                id_field = 'emp_code'
            else:
                continue
        elif 'email' in profile and profile['email'] not in ('INR', 'ABU','NSU'):
            id_field = 'email'
        elif 'mobile' in profile and profile['mobile'] not in ('INR', 'ABU','NSU'):
            id_field = 'mobile'
        else:
            vals = (json.dumps(profile), 'No suitable id field found' )
            cursor.execute(import_log_sql, vals)
            #connection.commit()
            print('No suitable id field found')
            continue
            
        spargs = [
            id_field,
            profile["widget_id"],
            profile["uuid"],
            profile["device_profile_id"],
            profile["widget_profile_id"],
            profile["full_name"],
            profile["email"],
            profile["mobile"],
            profile["id_number"],
            emp_code,
            None,
            profile["nbr_gates"],
            profile["pct_complete"],
            profile["nbr_correct"],
            profile["pct_correct"],
            profile["engagement_seconds"],
            profile["gate_seconds"],
            profile["start"],
            profile["end"],
            profile["lock_gate"],
            profile["video_seconds"],
            profile["video_pct"],
            profile["channels_text"],
            False,
            out
        ]
        #print (spargs)
        
        cursor.callproc('upsert_participant_results', spargs)
        #out = cursor.fetchone()
        #print (out)

        

except mysql.connector.Error as error:
    print("Failed to execute stored procedure: {}".format(error))
finally:
    if (connection.is_connected()):
        cursor.close()
        connection.close()
        print("MySQL connection is closed")
