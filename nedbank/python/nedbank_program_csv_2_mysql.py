# read csv file of personnel info, extract relevant data and exec 'upsert_participant' to populate participants.

import csv
import json

import mysql.connector
from mysql.connector import Error
from mysql.connector import errorcode

from configobj import ConfigObj
config = ConfigObj('config.ini')
print(config)

with open('programs_nedbank_3.csv') as csvfile:
    data = list(csv.reader(csvfile, delimiter=';'))

print(data[0])

#exc stored proc
try:
    connection = mysql.connector.connect(host = config['mysql']['host'],
                                         port = config['mysql']['port'],
                                         database = config['mysql']['database'],
                                         user = config['mysql']['user'],
                                         password = config['mysql']['password'],
                                         autocommit = True )

    cursor = connection.cursor()
    out = ''
    for doc in data:
        if doc[0] == "Company Name":
            continue
        pgm = {
            "program": doc[3],
            "module": doc[4]
        }
        pgmstr = json.dumps(pgm)
        spargs = [
            doc[2],
            None,
            None,
            None,
            None,
            int(doc[6]),
            int(doc[8]),
            None,
            None,
            int(doc[6]),
            int(doc[8]),
            doc[3],   #group_1
            doc[4],
            None,
            None,
            None,   #group_5
            pgmstr,   #program
            out
        ]
        cursor.callproc('upsert_journey', spargs)
        out = cursor.fetchone()
        print out

except mysql.connector.Error as error:
    print("Failed to execute stored procedure: {}".format(error))
finally:
    if (connection.is_connected()):
        cursor.close()
        connection.close()
        print("MySQL connection is closed")


# "Company Name";
# ;
# "Widget ID";
# "Program Name";
# "Module Number";
# "Gate Count Yes/No";
# "Gate count required";
# "Pass rate Yes/No";
# "Pass rate required"
                                                        