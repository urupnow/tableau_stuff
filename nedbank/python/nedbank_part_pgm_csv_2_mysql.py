# read csv file of personnel info, extract relevant data and exec 'upsert_participant' to populate participants.

import csv
import json
import sys

import mysql.connector
from mysql.connector import Error
from mysql.connector import errorcode

from configobj import ConfigObj
config = ConfigObj('../config.ini')

pgm_nbr = sys.argv[1]
print(config)

file_name = config['pgm_files'][pgm_nbr]
#file_name = 'participants_pgm_1_nedbank.csv'
print(file_name)

with open(file_name) as csvfile:
    data = list(csv.reader(csvfile, delimiter=';'))

#exc stored proc
try:
    connection = mysql.connector.connect(host = config['mysql']['host'],
                                         port = config['mysql']['port'],
                                         database = config['mysql']['database'],
                                         user = config['mysql']['user'],
                                         password = config['mysql']['password'],
                                         autocommit = True )


    cursor = connection.cursor()

    for doc in data:
        out = ''
        if doc[0] == "Company Name":
            continue
        spargs = [
            'emp_code',     #id_field
            str(doc[2]),    #participant_id
            str(doc[3]),    #program_name
            str(doc[4]),    #requirement
            out
        ]
        cursor.callproc('upsert_participant_program', spargs)
        res = cursor.fetchone()

except mysql.connector.Error as error:
    print("Failed to execute stored procedure: {}".format(error))
finally:
    if (connection.is_connected()):
        cursor.close()
        connection.close()
        print("MySQL connection is closed")


# "Company Name";
# "Email";
# "Employee number";
# "Program Name";
# "Required"

# call upsert_participant_program(
#     '181062', 
#     "Compliance training: Gifts, Entertainment & OBI",
#     'emp_code',
#     "Compulsory",
#     @msg
# );
# select @msg;