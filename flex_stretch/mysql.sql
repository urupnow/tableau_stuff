drop table attendees;

create table attendees(
    calendar_event_id   VARCHAR(255),
    email               VARCHAR(255),
    display_name        VARCHAR(255),
    response_status     VARCHAR(255),
    is_organizer           BIT,
    is_self                BIT,
    comment             VARCHAR(255)
);

drop table calendar_events;

create table calendar_events(
    id                  VARCHAR(255),
    start               TIMESTAMP,
    end                 TIMESTAMP,
    organizer           VARCHAR(255),
    status              VARCHAR(255),
    sequence            INT,
    description         VARCHAR(255)
);

drop view view_calendar_events;

create view view_calendar_events as 
    select  calendar_events.id as id,
            calendar_events.start as start,
            calendar_events.end as end,
            calendar_events.organizer as organizer,
            calendar_events.status as status,
            calendar_events.sequence as sequence,
            calendar_events.description as description,
            attendees.email as attendee_email,
            attendees.display_name as attendee_display_name,
            attendees.response_status as attendee_response,
            attendees.is_organizer as attendee_is_organizer,
            attendees.is_self as attendee_is_self,
            attendees.comment as comment
    from    calendar_events 
    left join    attendees on attendees.calendar_event_id = calendar_events.id;


drop procedure upsert_attendee;

create procedure upsert_attendee(
    IN  p_calendar_event_id     VARCHAR(255),
    IN  p_email                 VARCHAR(255),
    IN  p_dispay_name           VARCHAR(255),
    IN  p_response_status       VARCHAR(255),
    IN  p_is_organizer          BIT,
    IN  p_is_self               BIT,
    IN  p_comment               VARCHAR(255)
)
sp:     BEGIN
    DECLARE v_count     INT;

    select count(*) INTO v_count
    from attendees
    where calendar_event_id = p_calendar_event_id
    and   email = p_email;

    if v_count > 0 then
        update attendees
        set     display_name = coalesce(display_name, p_dispay_name),
                response_status = p_response_status,
                is_organizer = p_is_organizer,
                is_self = p_is_self,
                comment = p_comment
        where   calendar_event_id = p_calendar_event_id
        and     email = p_email;
    else
        insert into attendees(
            calendar_event_id,
            email,
            display_name,
            response_status,
            is_organizer,
            is_self,
            comment
        )
        values (
            p_calendar_event_id,
            p_email,
            p_dispay_name,
            p_response_status,
            p_is_organizer,
            p_is_self,
            p_comment
        );
	end if;
END;

drop procedure upsert_calendar_event;

create procedure upsert_calendar_event(
    IN  p_id                VARCHAR(255),
    IN  p_start             TIMESTAMP,
    IN  p_end               TIMESTAMP,
    IN  p_organizer      VARCHAR(255),
    IN  p_status            VARCHAR(255),
    IN  p_sequence          INT,
    IN  p_description       VARCHAR(255)
)
sp:     BEGIN
    DECLARE v_count     INT;

    select count(*) 
    from calendar_events
    where id = p_id;

    if v_count > 0 then
        update calendar_events
        set     start = p_start,
                end = p_end,
                organizer = p_organizer,
                status = p_status,
                sequence = p_sequence,
                description = p_description;
    else 
        insert into calendar_events (
            id,
            start,
            end,
            organizer,
            status,
            sequence,
            description
        )
        values(
            p_id,
            p_start,
            p_end,
            p_organizer,
            p_status,
            p_sequence,
            p_description
        );
    end if;


END;
call upsert_attendee(
    "_8ko42ea56cq36b9i60qjab9k74sj0b9o8grj4ba26or3ie22612jchhi6s_20200323T090000Z",
    "admin@urup.com",
    "",
    "accepted",
    true,
    false
);

call upsert_calendar_event(
"_8ko42ea56cq36b9i60qjab9k74sj0b9o8grj4ba26or3ie22612jchhi6s_20200323T090000Z",
"2020-03-23 09:00:00",
"2020-03-23 10:00:00",
"admin@urup.com", 
"confirmed",
1,
"Monday Morning Meeting"
);