from pymongo import MongoClient
from pymongo import errors
from bson import ObjectId
import csv

import sys
rel_data_path = sys.argv[1]

import os
base_path = os.path.dirname(__file__)
data_path = os.path.join(base_path, rel_data_path)
print(data_path)

from configobj import ConfigObj
config = ConfigObj(data_path + "config.ini")

import datetime
date_object = datetime.date.today()

# connect to MongoDB
uri = config['mongo']['url']

client = MongoClient(uri)
db=client.urup

# open the file for reading
file_name = data_path + config['files']['sub_folder'] + config['files']['division_rgm']
print(file_name)

with open(file_name) as csvfile:
    data = list(csv.reader(csvfile, delimiter=','))

def assign_arg(conf_item):
    try:
        if (len(conf_item) >= 0):
            return conf_item
        else: 
            return None
    except ValueError:
        return None


rownbr = 0
hdr = []
for line in data:
    # read a single line
    if rownbr == 0:
        hdr = line
        print(hdr)
    else:
        _emp_code = line[0]
        _div = line[3]

        for doc in db.org_parties.find({'organisation':'KFC','org_0': _div}):
            _ancestors = [_emp_code]

            try:
                db.org_parties.update_one(
                    {'organisation': doc['organisation'], 'emp_code': doc['emp_code'] },
                    {'$set': {'ancestors': _ancestors}},
                    upsert = False
                )
            except Exception as e:
                print (e)

    rownbr += 1

# close the pointer to that file