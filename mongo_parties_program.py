from pymongo import MongoClient
from bson import ObjectId
import csv
import sys

#import os
#os.chdir(os.path.dirname(__file__))
from configobj import ConfigObj
config = ConfigObj("config.ini")

import datetime
date_object = datetime.date.today()

# connect to MongoDB
uri = "mongodb://urupuser:Md5#M~{5cs#5zjnd@mongospreview.urup.com/urup:27017?authSource=admin"
#uri = "mongodb://urupuser:Md5#M~{5cs#5zjnd@34.245.121.178:27017?authSource=admin"

client = MongoClient(uri)
db=client.urup

# open the file for reading
pgm_nbr = sys.argv[1]
file_name = config['pgm_files'][pgm_nbr]
print(file_name)

with open(file_name) as csvfile:
    data = list(csv.reader(csvfile, delimiter=';'))

rownbr = 0
hdr = []

for line in data:
    # read a single line
    if rownbr == 0:
        hdr = line
        print(hdr)
    else:
        _org = line[0]
        _empc = line[1]
        _req = line[2]
        _pgm = line[3]
        print(_org + ' -' + _empc)

        pgm = {_pgm: _req}
        db.org_parties.update_one(
            {'organisation': _org, 'emp_code': _empc },
            {'$set': {
                'programs.'+_pgm : _req
            }},
            upsert = False
        )
    rownbr += 1

# close the pointer to that file
client.close()