from pymongo import MongoClient
from bson import ObjectId
import csv
import sys

def assign_arg(conf_item):
    try:
        if (len(conf_item) >= 0):
            return conf_item
        else: 
            return None
    except ValueError:
        return None

rel_data_path = sys.argv[1]

import os
base_path = os.path.dirname(__file__)
data_path = os.path.join(base_path, rel_data_path)
print(data_path)

from configobj import ConfigObj
config = ConfigObj(data_path + "config.ini")

import datetime
date_object = datetime.date.today()

# connect to MongoDB
uri = "mongodb://urupuser:Md5#M~{5cs#5zjnd@mongospreview.urup.com/urup:27017?authSource=admin"
#uri = "mongodb://urupuser:Md5#M~{5cs#5zjnd@34.245.121.178:27017?authSource=admin"

client = MongoClient(uri)
db=client.urup

# open the file for reading
file_name = data_path + config['files']['sub_folder'] + config['files']['programs']
print(file_name)

with open(file_name) as csvfile:
    data = list(csv.reader(csvfile, delimiter=';'))

rownbr = 0
hdr = []

for line in data:
    # read a single line
    if rownbr == 0:
        hdr = line
        print(hdr)
    else:
        doc={
            'organisation': assign_arg(line[int(config['program_field_map']['org'])]),
            'program_name': assign_arg(line[int(config['program_field_map']['program_name'])]),
            'module_name': assign_arg(line[int(config['program_field_map']['module_name'])]),
            'widget_id': assign_arg(line[int(config['program_field_map']['widget_id'])]),
            'count_gates': assign_arg(line[int(config['program_field_map']['count_gates'])]),
            'gate_count_nbr': assign_arg(line[int(config['program_field_map']['gate_count_nbr'])]),
            'pass_check': assign_arg(line[int(config['program_field_map']['pass_check'])]),
            'pass_pct': assign_arg(line[int(config['program_field_map']['pass_pct'])])
        }

        _wid = doc["widget_id"]
        print(_wid)

        db.widget_groups.update_one(
            {'widget_id': _wid },
            {'$set': doc},
            upsert = True
        )
    rownbr += 1

# close the pointer to that file
client.close()